// #include <cstdio>
#include <string>
#include <iostream>
#include <ctime>

#include "ProjectVersion.h"

#include "enigma/enigma.h"


void version() {
    std::cout << "Version " PROJECT_VERSION_MAJOR "." PROJECT_VERSION_MINOR
                 "." PROJECT_VERSION_PATCH
                 "\n"
                 "Git branch "
              << GIT_BRANCH " " GIT_COMMIT_HASH "\n\n";
}


int main(int argc, char* argv[]) {

    // TODO(ER) - Add command-line options

    version();

    std::cout << "Enigma!\n";

    int a = 3;
    int b = 5;
    int c = 7;

    std::string input;
    input.reserve(100);

    input = "Helloworld";


    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("I", 0, 0));
    rotors.push_back(Rotor("II", 0, 0));
    rotors.push_back(Rotor("III", 0, 0));

    Reflector reflector("B");

    Plugboard plugboard("");

    Enigma e(rotors, &reflector, &plugboard);


    e.rotorPosition({a, b, c});
    const std::string ciphertext = e.encrypt(input);

    std::cout << "Planetext: " << input << "\n";
    std::cout << "Ciphertext: " << ciphertext << "\n";

    return 0;
}

