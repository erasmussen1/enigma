# Cmake Demo Enigma Project

---
This project could be used as a template for any C/C++ project

---

## Key features:

 - Cmake is used to as build-system generator
 - Uses ccashe for debug builds (the can speed up build times significantly)
 - Build release builds
 - Gtest is the default unit-test framework
 - Debian install package
 - Switch to build with address sanitizer


