#include <string>
#include <vector>

#include <enigma/reflector.h>


Reflector::Reflector(const std::string& name) {
    std::string encoding;

    if (name == "B") {
        encoding = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
    } else if (name == "C") {
        encoding = "FVPJIAOYEDRZXWGCTKUQSBNMHL";
    } else {
        encoding = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
    }

    int i = 0;
    for (auto& ch : encoding) {
        wiring[i++] = ch - 'A';
    }
}

int Reflector::forward(int c) {
    return wiring[c];
}

