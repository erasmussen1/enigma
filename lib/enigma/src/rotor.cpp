#include <string>
#include <vector>
#include <algorithm>

#include <enigma/rotor.h>


Rotor::Rotor(const std::string& name, int rotorPosition, int ringSetting)
    : _name{name}, _rotorPosition{rotorPosition}, _ringSetting{ringSetting} {

    _notchPosition = 0;
    std::string encoding;

    if (_name == "I") {
        encoding = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
        _notchPosition = 16;
    } else if (_name == "II") {
        encoding = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
        _notchPosition = 4;
    } else if (_name == "III") {
        encoding = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
        _notchPosition = 21;
    } else if (_name == "IV") {
        encoding = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
        _notchPosition = 9;
    } else if (_name == "V") {
        encoding = "VZBRGITYUPSDNHLXAWMJQOFECK";
        _notchPosition = 25;
    } else if (_name == "VI") {
        encoding = "JPGVOUMFYQBENHZRDKASXLICTW";
    } else if (_name == "VII") {
        encoding = "NZJHGRCXMYSWBOUFAIVLPEKQDT";
    } else if (_name == "VIII") {
        encoding = "FKQHTLXOCBJSPDZRAMEWNIUYGV";
    } else {
        encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }

    for (int i = 0; i < (int)encoding.size(); i++) {
        _forwardWiring[i] = encoding[i] - 'A';

        int forward = _forwardWiring[i];
        _backwardWiring[forward] = i;
    }
}

inline int encipher(int k, int pos, int ring, int* mapping) {
    int shift = pos - ring;

    return (mapping[(k + shift + 26) % 26] - shift + 26) % 26;
}

void Rotor::setRotorPosition(int pos) {
    _rotorPosition = ((pos % 26) + 26) % 26;
}

int Rotor::forward(int c) {
    return encipher(c, _rotorPosition, _ringSetting, _forwardWiring);
}

int Rotor::backward(int c) {
    return encipher(c, _rotorPosition, _ringSetting, _backwardWiring);
}

bool Rotor::isAtNotch() {
    if ((_name == "VI") || (_name == "VII" ) || (_name == "VIII")) {
        return _rotorPosition == 12 || _rotorPosition == 25;
    }

    return _notchPosition == _rotorPosition;
}

void Rotor::turnover() {
    _rotorPosition = (_rotorPosition + 1) % 26;
}


