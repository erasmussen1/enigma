
#include <string>
#include <vector>

#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>

#include <enigma/plugboard.h>



Plugboard::Plugboard(const std::string& connections) {
    for (int i = 0; i < 26; i++) {
        wiring[i] = i;
    }

    if (connections.empty()) {
        return;
    }

    std::istringstream iss(connections);
    std::vector<std::string> tokens;

    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::back_inserter(tokens));

    for (auto& a : tokens) {
        if (a.size() != 2) {
            for (int i = 0; i < 26; i++) {
                wiring[i] = i;
            }
            break;
        }

        int c1 = a[0] - 'A';
        int c2 = a[1] - 'A';

        wiring[c1] = c2;
        wiring[c2] = c1;
    }
}

int Plugboard::forward(int c) {
    return wiring[c];
}

