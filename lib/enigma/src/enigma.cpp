#include <iostream>
#include <string>
#include <vector>

#include <ctype.h>

#include <enigma/plugboard.h>
#include <enigma/reflector.h>
#include <enigma/rotor.h>
#include <enigma/enigma.h>




Enigma::Enigma(std::vector<Rotor> &rotors, Reflector *reflector, Plugboard *plugboard) {
    // 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
    // A B C D E F G H I J K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z
    _leftRotor = &rotors[0];
    _middleRotor = &rotors[1];
    _rightRotor = &rotors[2];

    _reflector = reflector;
    _plugboard = plugboard;
}

void Enigma::rotate() {
    // If middle rotor notch - double-stepping
    if (_middleRotor->isAtNotch()) {
        _middleRotor->turnover();
        _leftRotor->turnover();
    }
    // If left-rotor notch
    else if (_rightRotor->isAtNotch()) {
        _middleRotor->turnover();
    }

    // Increment right-most rotor
    _rightRotor->turnover();
}

int Enigma::encrypt0(int c) {
        rotate();

        // Plugboard in
        int c0 = _plugboard->forward(c);

        // Right to left
        int c1 = _rightRotor->forward(c0);
        int c2 = _middleRotor->forward(c1);
        int c3 = _leftRotor->forward(c2);

        // Reflector
        int c4 = _reflector->forward(c3);

        // Left to right
        int c5 = _leftRotor->backward(c4);
        int c6 = _middleRotor->backward(c5);
        int c7 = _rightRotor->backward(c6);

        // Plugboard out
        int c8 = _plugboard->forward(c7);

        return c8;
}

void Enigma::rotorPosition(std::vector<int> rotorPos) {
    if (rotorPos.size() < 3) {
        return;
    }

    _leftRotor->setRotorPosition(rotorPos[0]);
    _middleRotor->setRotorPosition(rotorPos[1]);
    _rightRotor->setRotorPosition(rotorPos[2]);
}

char Enigma::encrypt(const char ch) {
    int upperCaseChar = toupper(ch);

    return (char)(encrypt0((int)(upperCaseChar - 'A')) + 'A');
}

std::string Enigma::encrypt(std::string input) {
    std::string output(input.size(), '*');

    for (size_t i = 0; i < input.size(); i++) {
        output[i] = Enigma::encrypt((char)input[i]);
    }

    return output;
}
