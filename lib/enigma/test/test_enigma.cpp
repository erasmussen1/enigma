#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>

#include <ctime>

#include <enigma/plugboard.h>
#include <enigma/reflector.h>
#include <enigma/rotor.h>
#include <enigma/enigma.h>

#include <gtest/gtest.h>


int l2n(char ch) {
    return ch - 'A';
}

char n2l(int i) {
    return i + 'A';
}


TEST(TestSuite, First_Value_In_Reflector_B) {
    Reflector reflector("B");

    EXPECT_EQ(n2l(reflector.forward(l2n('A'))), 'Y');
}

TEST(TestSuite, First_Rotor) {
    Rotor r1("I", 0, 0);

    EXPECT_TRUE(true);
}

TEST(TestSuite, Basic_Plugboard) {
    Plugboard plugboard("");

    for (int i = 0; i < 26; i++) {
        EXPECT_EQ(plugboard.forward(i), i);
    }
}

TEST(TestSuite, Basic_Plugboard_two) {
    Plugboard plugboard("AZ BQ");

    EXPECT_EQ( n2l(plugboard.forward(l2n('A'))), 'Z');
    EXPECT_EQ( n2l(plugboard.forward(l2n('B'))), 'Q');
}

TEST(TestSuite, Basic_Settings) {
    std::vector<Rotor> rotors;

    rotors.push_back(Rotor("I", 0, 0));
    rotors.push_back(Rotor("II", 0, 0));
    rotors.push_back(Rotor("III", 0, 0));

    Reflector reflector("B");

    Plugboard plugboard("");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({0, 0, 0});

    const std::string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const std::string output = "BJELRQZVJWARXSNBXORSTNCFMEYHCXTGYJFLINHNXSHIUNTHEORXOPLOVFEKAGADSPNPCMHRVZCYECDAZIHVYGPITMSRZKGGHLSRBLHL";

    std::string ciphertext = e.encrypt(input);

    EXPECT_EQ(output, ciphertext);
}

TEST(TestSuite, Varied_Rotors) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("VII", 0, 1));
    rotors.push_back(Rotor("V", 0, 2));
    rotors.push_back(Rotor("IV", 0, 3));

    Reflector reflector("B");

    Plugboard plugboard("");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({10, 5, 12});

    std::string input = "ABCDEFGHIJKLMNOPQRSTUVWXYZAAAAAAAAAAAAAAAAAAAAAAAAAABBBBBBBBBBBBBBBBBBBBBBBBBBABCDEFGHIJKLMNOPQRSTUVWXYZ";
    std::string output = "FOTYBPKLBZQSGZBOPUFYPFUSETWKNQQHVNHLKJZZZKHUBEJLGVUNIOYSDTEZJQHHAOYYZSENTGXNJCHEDFHQUCGCGJBURNSEDZSEPLQP";

    std::string ciphertext = e.encrypt(input);

    EXPECT_EQ(output, ciphertext);
}

TEST(TestSuite, Long_Input) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("III", 0, 11));
    rotors.push_back(Rotor("VI", 0, 13));
    rotors.push_back(Rotor("VIII", 0, 19));

    Reflector reflector("B");

    Plugboard plugboard("");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({3, 5, 9});

    std::string longInput;
    for (int i = 0; i < 500; i++) {
        longInput += 'A';
    }

    std::string output =
        "YJKJMFQKPCUOCKTEZQVXYZJWJFROVJMWJVXRCQYFCUVBRELVHRWGPYGCHVLBVJEVTTYVMWKJFOZHLJEXYXRDBEVEHV"
        "XKQSBPYZNIQDCBGTDDWZQWLHIBQNTYPIEBMNINNGMUPPGLSZCBRJULOLNJSOEDLOBXXGEVTKCOTTLDZPHBUFKLWSFS"
        "RKOMXKZELBDJNRUDUCO"
        "TNCGLIKVKMHHCYDEKFNOECFBWRIEFQQUFXKKGNTSTVHVITVHDFKIJIHOGMDSQUFMZCGGFZMJUKGDNDSNSJKWKENIRQ"
        "KSUUHJYMIG"
        "WWNMIESFRCVIBFSOUCLBYEEHMESHSGFDESQZJLTORNFBIFUWIFJTOPVMFQCFCFPYZOJFQRFQZTTTOECTDOOYTGVKEW"
        "PSZGHCTQRP"
        "GZQOVTTOIEGGHEFDOVSUQLLGNOOWGLCLOWSISUGSVIHWCMSIUUSBWQIGWEWRKQFQQRZHMQJNKQTJFDIJYHDFCWTHXU"
        "OOCVRCVYOHL";

    std::string ciphertext = e.encrypt(longInput);

    EXPECT_EQ(output, ciphertext);
}

TEST(TestSuite, Encrypt_and_Decrypt_text) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("I", 0, 0));
    rotors.push_back(Rotor("II", 0, 0));
    rotors.push_back(Rotor("III", 0, 0));

    Reflector reflector("B");

    Plugboard plugboard("");

    srand(time(NULL));

    std::string input;
    for (int i = 0; i < 1000; i++) {
        int r = rand() % 26;
        input += (char)(r + 'A');
    }

    std::vector<std::string> rotorNames = {"I", "IV", "V", "VI", "VII", "VIII"};

    int i = 0;
    for (auto& r : rotorNames) {
        i++;
        int a = 3 * i;
        int b = 5 * i;
        int c = 7 * i;

        {
            std::vector<Rotor> rotors;
            rotors.push_back(Rotor(r, 0, 0));
            rotors.push_back(Rotor("II", 0, 0));
            rotors.push_back(Rotor("III", 0, 0));

            Enigma e(rotors, &reflector, &plugboard);

            // Encrypt text
            e.rotorPosition({a, b, c});
            std::string ciphertext = e.encrypt(input);

            // Decrypt text
            e.rotorPosition({a, b, c});
            std::string plaintext = e.encrypt(ciphertext);

            EXPECT_EQ(input, plaintext);
        }
    }
}

TEST(TestSuite, Four_Plugs) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("I", 0, 0));
    rotors.push_back(Rotor("II", 0, 0));
    rotors.push_back(Rotor("III", 0, 0));

    Reflector reflector("B");

    Plugboard plugboard("AC FG JY LW");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({0, 0, 0});

    std::string input = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    std::string expectedOutput = "QREBNMCYZELKQOJCGJVIVGLYEMUPCURPVPUMDIWXPPWROOQEGI";

    std::string output = e.encrypt(input);

    EXPECT_EQ(expectedOutput, output);
}

TEST(TestSuite, Six_Plugs) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("IV", 0, 0));
    rotors.push_back(Rotor("VI", 0, 0));
    rotors.push_back(Rotor("III", 0, 0));

    Reflector reflector("B");

    Plugboard plugboard("BM DH RS KN GZ FQ");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({0, 10, 6});

    const std::string input = "WRBHFRROSFHBCHVBENQFAGNYCGCRSTQYAJNROJAKVKXAHGUZHZVKWUTDGMBMSCYQSKABUGRVMIUOWAPKCMHYCRTSDEYTNJLVWNQY";
    const std::string expectedOutput = "FYTIDQIBHDONUPAUVPNKILDHDJGCWFVMJUFNJSFYZTSPITBURMCJEEAMZAZIJMZAVFCTYTKYORHYDDSXHBLQWPJBMSSWIPSWLENZ";

    std::string output = e.encrypt(input);

    EXPECT_EQ(expectedOutput, output);
}

TEST(TestSuite, Ten_Plugs) {
    std::vector<Rotor> rotors;
    rotors.push_back(Rotor("I", 0, 5));
    rotors.push_back(Rotor("II", 0, 5));
    rotors.push_back(Rotor("III", 0, 4));

    Reflector reflector("B");

    Plugboard plugboard("AG HR YT KI FL WE NM SD OP QJ");

    Enigma e(rotors, &reflector, &plugboard);
    e.rotorPosition({0, 1, 20});

    std::string input = "RNXYAZUYTFNQFMBOLNYNYBUYPMWJUQSBYRHPOIRKQSIKBKEKEAJUNNVGUQDODVFQZHASHMQIHSQXICTSJNAUVZYIHVBBARPJADRH";
    std::string expectedOutput = "CFBJTPYXROYGGVTGBUTEBURBXNUZGGRALBNXIQHVBFWPLZQSCEZWTAWCKKPRSWOGNYXLCOTQAWDRRKBCADTKZGPWSTNYIJGLVIUQ";

    std::string output = e.encrypt(input);

    EXPECT_EQ(expectedOutput, output);
}

