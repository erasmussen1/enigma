#ifndef ROTOR_H__
#define ROTOR_H__

#include <string>
#include <vector>

#include <iostream>


struct Rotor {
    Rotor(const std::string& name, int rotorPosition, int ringSetting = 0);

    int forward(int c);
    int backward(int c);
    bool isAtNotch();
    void turnover();

    void setRotorPosition(int pos);

    std::string _name;

private:
    int _rotorPosition;
    int _notchPosition;
    int _ringSetting;

    int _forwardWiring[26];
    int _backwardWiring[26];
};

#endif

