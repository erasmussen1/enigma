#ifndef PLUGBOARD_H__
#define PLUGBOARD_H__

#include <string>


struct Plugboard {
    Plugboard(const std::string& connections);

    int forward(int c);

private:
    int wiring[26];
};

#endif  // PLUGBOARD_H__

