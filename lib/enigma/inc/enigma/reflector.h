#ifndef REFLECTOR_H__
#define REFLECTOR_H__

#include <string>
#include <vector>


struct Reflector {
    Reflector(const std::string& name);

    int forward(int c);

private:
    int wiring[26];
};

#endif
