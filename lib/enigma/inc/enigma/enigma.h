#ifndef __LIB_FIB__
#define __LIB_FIB__

#include <string>
#include <vector>

#include <enigma/plugboard.h>
#include <enigma/reflector.h>
#include <enigma/rotor.h>
#include <enigma/enigma.h>


/*
 *  Enigma I
 */
struct Enigma {
    Enigma(std::vector<Rotor> &rotors, Reflector *reflector, Plugboard *plugboard);

    std::string encrypt(std::string input);
    char encrypt(char ch);
    int encrypt0(int c);

    void rotate();

    void rotorPosition(std::vector<int> rotorPos);

private:
    Reflector* _reflector;
    Plugboard* _plugboard;

    Rotor* _leftRotor = nullptr;
    Rotor* _middleRotor = nullptr;
    Rotor* _rightRotor = nullptr;
};

#endif  // __LIB_FIB__
